Referral Candy
------

### Summary

This module helps you to connect with
["Referral Candy API"](https://answers.referralcandy.com/en/articles/2457871-api-integration).

### Basic configuration

The intended audience is module developers or web site programmers.
The included submodules provides integration with commerce module. However the tracking
library must be added manually in your theme.

To enable referralcandy:

1. Enable the module
2. Go to the admin interface (admin/config/referralcandy/settings).
3. In the form add **Access ID** and **Secret KEY**.
4. Use the service \Drupal::service('referralcandy.api')->->doRequest('verify');
5. Check the ReferralCandyTestForm for an example of usage.

Available methods are:
* 'verify'
* 'referrals'
* 'referrer'
* 'contacts'
* 'rewards'
* 'purchase'
* 'referral'
* 'signup'
* 'invite'
* 'unsubscribed'

***See: <https://www.referralcandy.com/api/>***
