<?php

namespace Drupal\referralcandy_commerce\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\referralcandy\ReferralCandyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Trigger Referral Candy when the order transitions to Fulfillment.
 */
class OrderFulfillmentSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The referral candy api connector.
   *
   * @var \Drupal\referralcandy\ReferralCandyInterface
   */
  protected $referralCandy;

  /**
   * The corresponding request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new Referral Candy object.
   *
   * @param \Drupal\referralcandy\ReferralCandyInterface $referralcandy
   *   The referral candy api.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The corresponding request stack.
   */
  public function __construct(ReferralCandyInterface $referralcandy, RequestStack $request_stack) {
    $this->referralCandy = $referralcandy;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // The format for adding a state machine event to subscribe to is:
    // {group}.{transition key}.pre_transition or
    // {group}.{transition key}.post_transition
    // depending on when you want to react.
    $events = ['commerce_order.place.post_transition' => 'onOrderPlace'];
    return $events;
  }

  /**
   * Sends purchase to Referral Candy.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function onOrderPlace(WorkflowTransitionEvent $event) {
    // Get the order entity.
    $order = $event->getEntity();

    $customer = [];

    // Define the customer details.
    if ($order) {
      $customer['order_id'] = $order->getOrderNumber();
      $customer['order_timestamp'] = $order->getCreatedTime();
      $customer['email'] = $order->getEmail();
      $customer['total_price'] = $order->getTotalPrice()->getNumber();
      $customer['currency_code'] = $order->getTotalPrice()->getCurrencyCode();
      $billing_profile = $order->getBillingProfile();
      if ($billing_profile && $billing_profile->address) {
        $address = $billing_profile->address->first();
        $customer['first_name'] = $address->getGivenName();
        $customer['last_name'] = $address->getFamilyName();
      }

      $customer['ip_address'] = $this->requestStack->getCurrentRequest()->getClientIp();
      $customer['http_user_agent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
    }

    // Add query parameters.
    if (!empty($customer)) {

      $params = [
        'first_name' => $customer['first_name'],
        'last_name' => $customer['last_name'],
        'email' => $customer['email'],
        'locale' => 'en',
        'accepts_marketing' => 'false',
        'order_timestamp' => $customer['order_timestamp'],
        'browser_ip' => $customer['ip_address'],
        'user_agent' => $customer['http_user_agent'],
        'invoice_amount' => $customer['total_price'],
        'currency_code' => $customer['currency_code'],
        'external_reference_id' => $customer['order_id'],
      ];

      $this->referralCandy->doRequest('purchase', $params);
    }

  }

}
