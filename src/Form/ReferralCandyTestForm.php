<?php

namespace Drupal\referralcandy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\referralcandy\ReferralCandyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Referral Candy test form.
 */
class ReferralCandyTestForm extends FormBase {

  /**
   * The referral candy api connector.
   *
   * @var \Drupal\referralcandy\ReferralCandyInterface
   */
  protected $referralCandy;

  /**
   * Constructs a new Referral Candy object.
   *
   * @param \Drupal\referralcandy\ReferralCandyInterface $referralcandy
   *   The referral candy api.
   */
  public function __construct(ReferralCandyInterface $referralcandy) {
    $this->referralCandy = $referralcandy;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('referralcandy.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'referralcandy_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#tree'] = TRUE;

    $form['description'] = [
      '#markup' => '<p>' . $this->t('This page allows you to verify connection with Referral Candy API.') . '</p>',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $result = $this->referralCandy->doRequest('verify');
    if ($result['success']) {
      $this->messenger()->addMessage($this->t('Successfully verified with the message: %message .', ['%message' => $result['response']['message']]));
    }
    else {
      if ($result['error_msg']) {
        $this->messenger()->addMessage($this->t('Failed to reach API with the message: %message .', ['%message' => $result['error_msg']]));
      }
      else {
        $this->messenger()->addMessage($this->t('Failed to reach API with the message, check watchdog logs for more details.'));
      }

    }

  }

}
