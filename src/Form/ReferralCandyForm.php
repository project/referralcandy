<?php

namespace Drupal\referralcandy\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ReferralCandyForm.
 */
class ReferralCandyForm extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'referralcandy';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'referralcandy.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('referralcandy.settings');

    $form['referralcandy'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Referral Candy API Credentials'),
      '#description' => $this->t('Password field needs to be entered every time this forms gets submitted.'),
      '#weight' => '0',
    ];

    $form['referralcandy']['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#description' => $this->t('The App ID provided by the Referral Candy (To find out the API keys click follow this link https://my.referralcandy.com/settings and check Plugin tokens).'),
      '#default_value' => $config->get('app_id') ?: NULL,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['referralcandy']['access_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access ID'),
      '#description' => $this->t('The Access ID provided by the Referral Candy (To find out the API keys click follow this link https://my.referralcandy.com/settings and check API tokens).'),
      '#default_value' => $config->get('access_id') ?: NULL,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];

    $form['referralcandy']['secret_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Secret KEY'),
      '#description' => $this->t('The Access KEY provided by the Referral Candy (To find out the API keys click follow this link https://my.referralcandy.com/settings and check API tokens).'),
      '#default_value' => $config->get('secret_key') ?: NULL,
      '#attributes' => [
        'autocomplete' => 'off',
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('referralcandy.settings');
    $config->set('app_id', $form_state->getValue('app_id'));
    $config->set('access_id', $form_state->getValue('access_id'));
    $config->set('secret_key', $form_state->getValue('secret_key'));
    $config->save();
  }

}
