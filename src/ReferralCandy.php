<?php

namespace Drupal\referralcandy;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Service class to integrate with Referral Candy.
 */
class ReferralCandy implements ReferralCandyInterface {

  use StringTranslationTrait;

  const API_URL = 'https://my.referralcandy.com/api/v1/';

  const MESSAGE_SUCCESS = 'Success';

  /**
   * Available methods.
   *
   * @var methods
   */
  public static $methods = [
    'verify' => ['http_method' => 'get'],
    'referrals' => ['http_method' => 'get'],
    'referrer' => ['http_method' => 'get'],
    'contacts' => ['http_method' => 'get'],
    'rewards' => ['http_method' => 'get'],
    'purchase' => ['http_method' => 'post'],
    'referral' => ['http_method' => 'post'],
    'signup' => ['http_method' => 'post'],
    'invite' => ['http_method' => 'post'],
    'unsubscribed' => ['http_method' => 'put'],
  ];

  /**
   * The Logger Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $logger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The fetch url configured in the update settings.
   *
   * @var string
   */
  protected $accessId;

  /**
   * The fetch url configured in the update settings.
   *
   * @var string
   */
  protected $secretKey;

  /**
   * Constructs a new Referral Candy object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $client
   *   The client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $client, LoggerChannelFactoryInterface $logger_factory) {
    $this->configFactory = $config_factory->get('referralcandy.settings');
    $this->accessId = $config_factory->get('referralcandy.settings')->get('access_id');
    $this->secretKey = $config_factory->get('referralcandy.settings')->get('secret_key');
    $this->httpClient = $client;
    $this->logger = $logger_factory->get('referralcandy');
  }

  /**
   * API Request.
   *
   * @param string $method
   *   API available methods, check $this->methods.
   * @param array $params
   *   Query Parameters.
   *
   * @return array
   *   The response.
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function doRequest($method, array $params = []) {
    $return = [
      'success' => FALSE,
      'error_msg' => '',
      'http_code' => 200,
      'response' => [],
    ];
    if (!array_key_exists($method, self::$methods)) {
      $this->logger->warning('Method %method is not supported.', ['%method' => $method]);
      $return['error_msg'] = $this->t('Method %method is not supported.', ['%method' => $method]);
      return $return;
    }

    $url = self::API_URL . $method . '.json?';

    $options = [
      'query' => $this->addSignature($params),
    ];

    try {
      $response_string = $this->httpClient->request(self::$methods[$method]['http_method'], $url, $options);
      $response = $response_string->getBody()->getContents();

    }
    catch (RequestException $e) {
      $this->logger->warning('RequestException - Failed to reach API due to "%error".', ['%error' => $e->getMessage()]);
      // Handle 4xx and 5xx http responses.
      if ($response = $e->getResponse()) {
        $return['error_msg'] = $response->getReasonPhrase();
        $return['http_code'] = $response->getStatusCode();
        $this->logger->warning('RequestException - HTTP request to @url failed with error: @error.', [
          '@url' => $url,
          '@error' => $response->getStatusCode() . ' ' . $response->getReasonPhrase(),
        ]);
      }
    }
    catch (\Exception $e) {
      $return['error_msg'] = $e->getMessage();
      if ($e->getCode()) {
        $return['http_code'] = $e->getCode();
      }
      $this->logger->warning('Exception - Failed to reach API due to "%error".', ['%error' => $return]);
      return $return;
    }

    try {
      $response = $this->jsonDecode($response);
    }
    catch (\Exception $e) {
      $return['error_msg'] = $e->getMessage();
      return $return;
    }

    if (!is_array($response) || !$response) {
      if (empty($return['error_msg'])) {
        $return['error_msg'] = $this->t('Empty response from API.');
      }
    }
    else {
      $return['success'] = TRUE;
      $return['response'] = $response;
    }

    return $return;
  }

  /**
   * Add signature to parameters.
   */
  protected function addSignature($params) {
    $params['timestamp'] = time();
    $params['accessID'] = $this->accessId;
    ksort($params);
    $params['signature'] = $this->signature($params);

    return $params;
  }

  /**
   * Calculate signature.
   */
  protected function signature($params) {

    $collected_params = '';
    foreach ($params as $name => $value) {
      $collected_params .= $name . "=" . $value;
    }

    return md5($this->secretKey . $collected_params);
  }

  /**
   * Decode JSON.
   */
  public function jsonDecode($jsong_string) {
    if (!is_string($jsong_string)) {
      return [];
    }

    return Json::decode($jsong_string);
  }

}
