<?php

namespace Drupal\referralcandy;

/**
 * Provides a handler for IPN requests from PayPal.
 */
interface ReferralCandyInterface {

  /**
   * API Request.
   *
   * @param string $method
   *   API available methods, check $this->methods.
   * @param array $params
   *   Query Parameters.
   *
   * @return array
   *   The response.
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function doRequest($method, array $params = []);

}
